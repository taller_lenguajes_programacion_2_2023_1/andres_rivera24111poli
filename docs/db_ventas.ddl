-- Generado por Oracle SQL Developer Data Modeler 22.2.0.165.1149
--   en:        2023-02-15 19:16:04 COT
--   sitio:      Oracle Database 12c
--   tipo:      Oracle Database 12c



DROP TABLE persona CASCADE CONSTRAINTS;

DROP TABLE producto CASCADE CONSTRAINTS;

DROP TABLE venta CASCADE CONSTRAINTS;

-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE TABLE persona (
    id_persona VARCHAR2(15) NOT NULL,
    nombre     VARCHAR2(100) NOT NULL,
    apellido   VARCHAR2(100) NOT NULL,
    correo     VARCHAR2(100) NOT NULL,
    celular    INTEGER NOT NULL,
    fecha      DATE NOT NULL,
    password   VARCHAR2(20) NOT NULL
);

ALTER TABLE persona ADD CONSTRAINT persona_pk PRIMARY KEY ( id_persona );

CREATE TABLE producto (
    id_producto VARCHAR2(15) NOT NULL,
    nombre      VARCHAR2(100) NOT NULL,
    color       VARCHAR2(100) NOT NULL,
    detalle     VARCHAR2(200),
    imagen      VARCHAR2(1000)
);

ALTER TABLE producto ADD CONSTRAINT producto_pk PRIMARY KEY ( id_producto );

CREATE TABLE venta (
    fecha         VARCHAR2(100) NOT NULL,
    valor         FLOAT,
    id_venta      INTEGER NOT NULL,
    cantidad      INTEGER NOT NULL,
    id_persona_1  VARCHAR2(15) NOT NULL,
    id_producto_1 VARCHAR2(15) NOT NULL
);

ALTER TABLE venta ADD CONSTRAINT venta_pk PRIMARY KEY ( id_venta );

ALTER TABLE venta
    ADD CONSTRAINT fk_id_persona FOREIGN KEY ( id_persona_1 )
        REFERENCES persona ( id_persona );

ALTER TABLE venta
    ADD CONSTRAINT fk_id_producto FOREIGN KEY ( id_producto_1 )
        REFERENCES producto ( id_producto );



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             3
-- CREATE INDEX                             0
-- ALTER TABLE                              5
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- TSDP POLICY                              0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
