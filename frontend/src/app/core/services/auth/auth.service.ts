import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { Login } from '../../interfaces/login';

@Injectable()
export class AuthService {
  
  private url: string = 'https://dummyjson.com/auth/login';
  private headers: HttpHeaders = new HttpHeaders({'Content-type': 'application/json'});

  constructor(
		public _http: HttpClient,
		private router: Router
	) { }

  public auth(body: Login): Observable<any>{

		const httpOptions = {
		  headers: this.headers
		};
    
		return this._http.post(this.url, body, httpOptions);
	}
}
