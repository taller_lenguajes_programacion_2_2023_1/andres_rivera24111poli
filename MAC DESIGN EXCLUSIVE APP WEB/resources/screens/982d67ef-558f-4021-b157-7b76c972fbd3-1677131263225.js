jQuery("#simulation")
  .on("click", ".s-982d67ef-558f-4021-b157-7b76c972fbd3 .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Hotspot_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Image_2" ],
                    "top": {
                      "type": "nomove"
                    },
                    "left": {
                      "type": "movetoposition",
                      "value": "996.0"
                    },
                    "containment": false
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Hotspot_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Image_2" ],
                    "top": {
                      "type": "nomove"
                    },
                    "left": {
                      "type": "movetoposition",
                      "value": "946.0"
                    },
                    "containment": false
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Hotspot_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Image_2" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Rectangle_7")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_11 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_10 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_9 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_8 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_7 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CCCCCC",
                      "border-right-color": "#CCCCCC",
                      "border-bottom-color": "#CCCCCC",
                      "border-left-color": "#CCCCCC"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_7 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#1EAAF1",
                      "border-right-color": "#1EAAF1",
                      "border-bottom-color": "#1EAAF1",
                      "border-left-color": "#1EAAF1"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Rectangle_8")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_11 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_10 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_9 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_8 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_7 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CCCCCC",
                      "border-right-color": "#CCCCCC",
                      "border-bottom-color": "#CCCCCC",
                      "border-left-color": "#CCCCCC"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_8 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#1EAAF1",
                      "border-right-color": "#1EAAF1",
                      "border-bottom-color": "#1EAAF1",
                      "border-left-color": "#1EAAF1"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Rectangle_9")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_11 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_10 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_9 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_8 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_7 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CCCCCC",
                      "border-right-color": "#CCCCCC",
                      "border-bottom-color": "#CCCCCC",
                      "border-left-color": "#CCCCCC"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_9 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#1EAAF1",
                      "border-right-color": "#1EAAF1",
                      "border-bottom-color": "#1EAAF1",
                      "border-left-color": "#1EAAF1"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Rectangle_10")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_11 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_10 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_9 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_8 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_7 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CCCCCC",
                      "border-right-color": "#CCCCCC",
                      "border-bottom-color": "#CCCCCC",
                      "border-left-color": "#CCCCCC"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_10 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#1EAAF1",
                      "border-right-color": "#1EAAF1",
                      "border-bottom-color": "#1EAAF1",
                      "border-left-color": "#1EAAF1"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Rectangle_11")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_11 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_10 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_9 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_8 > .borderLayer",
                  "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_7 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CCCCCC",
                      "border-right-color": "#CCCCCC",
                      "border-bottom-color": "#CCCCCC",
                      "border-left-color": "#CCCCCC"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Rectangle_11 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#1EAAF1",
                      "border-right-color": "#1EAAF1",
                      "border-bottom-color": "#1EAAF1",
                      "border-left-color": "#1EAAF1"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Button_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Button_1" ],
                    "attributes": {
                      "opacity": "0.7"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-982d67ef-558f-4021-b157-7b76c972fbd3 #s-Button_1" ],
                    "attributes": {
                      "opacity": "1.0"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 200
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/8c1a3b77-3226-47a6-9cf9-c27139637a22"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/08c75814-8a8a-4d7a-8f08-9c48a510b347"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_5")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  });