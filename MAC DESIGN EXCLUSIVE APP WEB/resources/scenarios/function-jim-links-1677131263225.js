(function(window, undefined) {

  var jimLinks = {
    "1857fcde-c833-4987-a9f0-6097862bafb8" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "d5c25184-5e53-413e-8d43-31e5c5ffb3a7" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "508d70ce-a2cb-48a2-80e6-24dc10000e3f" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "ff1549a9-413c-4603-b43c-4ce442e07057" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7" : {
      "Button_1" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Button_3" : [
        "a8b4b8e9-c14a-4a47-9bac-3b9cdd74e5bf",
        "a8b4b8e9-c14a-4a47-9bac-3b9cdd74e5bf"
      ],
      "Paragraph_1" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "982d67ef-558f-4021-b157-7b76c972fbd3" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "8c1a3b77-3226-47a6-9cf9-c27139637a22" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "897a6b60-a487-4e28-b43a-4724269443d6" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "1cc9e28e-6ed1-451a-afc2-440c1109515b" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "79952e79-c290-4b1d-98a2-8e8a131dcfcd" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "08c75814-8a8a-4d7a-8f08-9c48a510b347" : {
      "Image_9" : [
        "1cc9e28e-6ed1-451a-afc2-440c1109515b"
      ],
      "Paragraph_29" : [
        "1cc9e28e-6ed1-451a-afc2-440c1109515b"
      ],
      "Paragraph_30" : [
        "1cc9e28e-6ed1-451a-afc2-440c1109515b"
      ],
      "Paragraph_31" : [
        "1cc9e28e-6ed1-451a-afc2-440c1109515b"
      ],
      "Image_8" : [
        "a15e83d9-1e2f-402a-8adc-e4ee102c1945"
      ],
      "Paragraph_26" : [
        "a15e83d9-1e2f-402a-8adc-e4ee102c1945"
      ],
      "Paragraph_27" : [
        "a15e83d9-1e2f-402a-8adc-e4ee102c1945"
      ],
      "Paragraph_28" : [
        "a15e83d9-1e2f-402a-8adc-e4ee102c1945"
      ],
      "Image_7" : [
        "897a6b60-a487-4e28-b43a-4724269443d6"
      ],
      "Paragraph_23" : [
        "897a6b60-a487-4e28-b43a-4724269443d6"
      ],
      "Paragraph_24" : [
        "897a6b60-a487-4e28-b43a-4724269443d6"
      ],
      "Paragraph_25" : [
        "897a6b60-a487-4e28-b43a-4724269443d6"
      ],
      "Image_6" : [
        "79952e79-c290-4b1d-98a2-8e8a131dcfcd"
      ],
      "Paragraph_20" : [
        "79952e79-c290-4b1d-98a2-8e8a131dcfcd"
      ],
      "Paragraph_21" : [
        "79952e79-c290-4b1d-98a2-8e8a131dcfcd"
      ],
      "Paragraph_22" : [
        "79952e79-c290-4b1d-98a2-8e8a131dcfcd"
      ],
      "Image_5" : [
        "982d67ef-558f-4021-b157-7b76c972fbd3"
      ],
      "Paragraph_17" : [
        "982d67ef-558f-4021-b157-7b76c972fbd3"
      ],
      "Paragraph_18" : [
        "982d67ef-558f-4021-b157-7b76c972fbd3"
      ],
      "Paragraph_19" : [
        "982d67ef-558f-4021-b157-7b76c972fbd3"
      ],
      "Image_4" : [
        "1857fcde-c833-4987-a9f0-6097862bafb8"
      ],
      "Paragraph_14" : [
        "1857fcde-c833-4987-a9f0-6097862bafb8"
      ],
      "Paragraph_15" : [
        "1857fcde-c833-4987-a9f0-6097862bafb8"
      ],
      "Paragraph_16" : [
        "1857fcde-c833-4987-a9f0-6097862bafb8"
      ],
      "Image_2" : [
        "508d70ce-a2cb-48a2-80e6-24dc10000e3f"
      ],
      "Paragraph_8" : [
        "508d70ce-a2cb-48a2-80e6-24dc10000e3f"
      ],
      "Paragraph_9" : [
        "508d70ce-a2cb-48a2-80e6-24dc10000e3f"
      ],
      "Paragraph_13" : [
        "508d70ce-a2cb-48a2-80e6-24dc10000e3f"
      ],
      "Image_3" : [
        "ff1549a9-413c-4603-b43c-4ce442e07057"
      ],
      "Paragraph_10" : [
        "ff1549a9-413c-4603-b43c-4ce442e07057"
      ],
      "Paragraph_11" : [
        "ff1549a9-413c-4603-b43c-4ce442e07057"
      ],
      "Paragraph_12" : [
        "ff1549a9-413c-4603-b43c-4ce442e07057"
      ],
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "f9c98e0f-46ca-46bc-b177-0b32dbd8908a" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "a15e83d9-1e2f-402a-8adc-e4ee102c1945" : {
      "Paragraph_2" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_6" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    },
    "a8b4b8e9-c14a-4a47-9bac-3b9cdd74e5bf" : {
      "Paragraph_1" : [
        "8c1a3b77-3226-47a6-9cf9-c27139637a22"
      ],
      "Paragraph_3" : [
        "08c75814-8a8a-4d7a-8f08-9c48a510b347"
      ],
      "Paragraph_4" : [
        "d5c25184-5e53-413e-8d43-31e5c5ffb3a7"
      ],
      "Paragraph_5" : [
        "f9c98e0f-46ca-46bc-b177-0b32dbd8908a"
      ],
      "Paragraph_9" : [
        "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7"
      ],
      "Image_1" : [
        "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);