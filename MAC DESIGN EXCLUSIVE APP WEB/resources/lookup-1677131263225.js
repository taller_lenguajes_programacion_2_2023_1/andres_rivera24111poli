(function(window, undefined) {
  var dictionary = {
    "1857fcde-c833-4987-a9f0-6097862bafb8": "DETALLE PRODUCTO 100794",
    "d5c25184-5e53-413e-8d43-31e5c5ffb3a7": "CONTACTANOS",
    "508d70ce-a2cb-48a2-80e6-24dc10000e3f": "DETALLE PRODUCTO 100465",
    "ff1549a9-413c-4603-b43c-4ce442e07057": "DETALLE PRODUCTO 100350",
    "82bfe7e1-3a6f-49a7-88b3-201e9c50dad7": "LOGIN",
    "982d67ef-558f-4021-b157-7b76c972fbd3": "DETALLE PRODUCTO 100367",
    "8c1a3b77-3226-47a6-9cf9-c27139637a22": "NOSOTROS",
    "897a6b60-a487-4e28-b43a-4724269443d6": "DETALLE PRODUCTO 100973",
    "1cc9e28e-6ed1-451a-afc2-440c1109515b": "DETALLE PRODUCTO 100143",
    "79952e79-c290-4b1d-98a2-8e8a131dcfcd": "DETALLE PRODUCTO 100354",
    "08c75814-8a8a-4d7a-8f08-9c48a510b347": "PRODUCTOS",
    "6f1cb80e-6fb3-4b0e-aa89-6dbdcc66cedd": "CARRITO DE COMPRAS",
    "f9c98e0f-46ca-46bc-b177-0b32dbd8908a": "CONSULTA",
    "a15e83d9-1e2f-402a-8adc-e4ee102c1945": "DETALLE PRODUCTO 100475",
    "a8b4b8e9-c14a-4a47-9bac-3b9cdd74e5bf": "REGISTRO",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);