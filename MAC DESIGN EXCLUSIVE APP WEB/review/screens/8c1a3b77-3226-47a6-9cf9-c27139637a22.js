var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1366" deviceHeight="325">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1024" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677131263225.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-8c1a3b77-3226-47a6-9cf9-c27139637a22" class="screen growth-vertical devWeb canvas PORTRAIT firer commentable non-processed" alignment="left" name="NOSOTROS" width="1366" height="325">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/8c1a3b77-3226-47a6-9cf9-c27139637a22-1677131263225.css" />\
      <div class="freeLayout">\
      <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Background"   datasizewidth="1366.0px" datasizeheight="220.0px" datasizewidthpx="1366.0" datasizeheightpx="219.9569528246875" dataX="0.5" dataY="-0.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_1_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_1" class="richtext manualfit firer ie-background commentable non-processed" customid="Acme"   datasizewidth="239.8px" datasizeheight="78.0px" dataX="585.3" dataY="71.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_1_0">MAC DESGIN</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Footer" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Path_2" class="path firer commentable non-processed" customid="devider"   datasizewidth="1206.0px" datasizeheight="1.0px" dataX="99.0" dataY="1185.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="1206.0" height="0.758331298828125" viewBox="99.0 1184.9999999999998 1206.0 0.758331298828125" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_2-8c1a3" d="M99.0 1184.9999999999998 L1305.0000000000014 1184.9999999999998 L1305.0000000000014 1185.758333333333 L99.0 1185.758333333333 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_2-8c1a3" fill="#D9D9D9" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_3" class="path firer commentable non-processed" customid="bg"   datasizewidth="1206.0px" datasizeheight="54.6px" dataX="99.0" dataY="1345.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="1206.0" height="54.600006103515625" viewBox="99.0 1345.0 1206.0 54.600006103515625" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_3-8c1a3" d="M99.0 1345.0 L1305.0000000000014 1345.0 L1305.0000000000014 1399.6000000000004 L99.0 1399.6000000000004 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_3-8c1a3" fill="#EFEFF4" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_7" class="richtext manualfit firer ie-background commentable non-processed" customid="Copyright"   datasizewidth="223.1px" datasizeheight="42.0px" dataX="99.0" dataY="1351.1" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_7_0">&copy; All rights reserved</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="Social" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_4" class="path firer commentable non-processed" customid="facebook_icn"   datasizewidth="18.7px" datasizeheight="30.5px" dataX="1271.3" dataY="1357.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="18.7197265625" height="30.465087890625" viewBox="1271.2728433228322 1357.0598743706335 18.7197265625 30.465087890625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_4-8c1a3" d="M1283.7206211751952 1387.5249687636085 L1283.7206211751952 1372.2906645288344 L1289.2586126269541 1372.2906645288344 L1289.9925095623405 1367.0408246836269 L1283.7206211751952 1367.0408246836269 L1283.7300241889404 1364.413224550974 C1283.7300241889404 1363.0439915104123 1283.9013530961597 1362.310321739003 1286.4911691712189 1362.310321739003 L1289.9533033343002 1362.310321739003 L1289.9533033343002 1357.0598862915624 L1284.414524107771 1357.0598862915624 C1277.7615650399723 1357.0598862915624 1275.4198868433969 1359.606621718854 1275.4198868433969 1363.8894303182312 L1275.4198868433969 1367.0414122153693 L1271.2728709793873 1367.0414122153693 L1271.2728709793873 1372.29126658746 L1275.4198868433969 1372.29126658746 L1275.4198868433969 1387.5249687636085 L1283.7206211751952 1387.5249687636085 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_4-8c1a3" fill="#1B2437" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_5" class="path firer commentable non-processed" customid="instagram_icn"   datasizewidth="31.2px" datasizeheight="27.1px" dataX="1040.8" dataY="1358.8"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="31.2451171875" height="27.080078125" viewBox="1040.8404297266763 1358.7523909181791 31.2451171875 27.080078125" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_5-8c1a3" d="M1072.0061442512592 1366.7140271000703 C1071.9328756105808 1365.275154600029 1071.6643244950872 1364.2859464520789 1071.279696722683 1363.4287812208893 C1070.8828817207377 1362.5189181048522 1070.272354411529 1361.7043222923503 1069.472481804732 1361.0271443435906 C1068.691042755087 1360.339431982976 1067.7447855797734 1359.805043616745 1066.7069789194702 1359.466557541125 C1065.7119802754007 1359.1332381938203 1064.5763778024987 1358.9005554476894 1062.91601304099 1358.8371157393685 C1061.2429954620147 1358.768302698164 1060.7119868457978 1358.7523909181793 1056.4690617314625 1358.7523909181793 C1052.2261198559622 1358.7523909181793 1051.6950479197872 1358.768302698164 1050.028356749545 1358.8317432135339 C1048.3678532428341 1358.8951829218545 1047.2262828639014 1359.1280730796032 1046.2373294134575 1359.46118501529 C1045.1870887620146 1359.805043616745 1044.24703321787 1360.334057035994 1043.4655476094313 1361.0271443435906 C1042.6719064314307 1361.7043222923503 1042.055454050281 1362.524293051834 1041.6645948490807 1363.4236088432308 C1041.2799372790491 1364.2859464520789 1041.0114150300064 1365.269779653047 1040.9382041222318 1366.7086521530882 C1040.858792514463 1368.1582753541427 1040.8404297266763 1368.618474118089 1040.8404297266763 1372.2951363878772 C1040.8404297266763 1375.9717970435677 1040.858792514463 1376.431887662935 1040.932003422238 1377.8762037091315 C1041.0052143300134 1379.3151044558906 1041.2739758912494 1380.3043400435101 1041.6586334612805 1381.161422955692 C1042.055454050281 1382.0713127043496 1042.6719064314307 1382.8858834983293 1043.4655476094313 1383.563020287585 C1044.24703321787 1384.250852091466 1045.1932903931834 1384.7851444188545 1046.2311268511128 1385.1236434072598 C1047.2262828639014 1385.4569998788231 1048.3616506804892 1385.689617253977 1050.0223897746905 1385.7531191050784 C1051.688846288618 1385.816759768624 1052.2201677799212 1385.83246494404 1056.4631245542353 1385.83246494404 C1060.70604966857 1385.83246494404 1061.2370601471384 1385.816759768624 1062.9038276738008 1385.7531191050784 C1064.5643470105013 1385.689617253977 1065.7058866606303 1385.4569998788231 1066.694791689929 1385.1236434072598 C1068.7950886199992 1384.4201031997666 1070.4556060943476 1382.9812024530072 1071.267509493142 1381.161422955692 C1071.6519808280036 1380.2990587142233 1071.9206883810402 1379.3151044558906 1071.9939570217184 1377.8762037091315 C1072.0670729495566 1376.431887662935 1072.0855065067071 1375.9717970435677 1072.0855065067071 1372.2951363878772 C1072.0855065067071 1368.618474118089 1072.0792564543942 1368.1582753541427 1072.0061442512592 1366.7140271000703 Z M1069.1918998972405 1377.7704560660297 C1069.1245684337896 1379.0929109268661 1068.8682008231342 1379.8071493771813 1068.6546412287091 1380.2832147263628 C1068.1294096274737 1381.4629590954823 1067.0489551131855 1382.3992522623778 1065.6874531034796 1382.854334335053 C1065.1380072054073 1383.0394262030177 1064.307983124548 1383.2616165038467 1062.7875964416996 1383.3197046696089 C1061.1392624721882 1383.3833421049585 1060.6449645327298 1383.3990505085708 1056.475309921424 1383.3990505085708 C1052.3055286702026 1383.3990505085708 1051.8049825407827 1383.3833421049585 1050.1626174084784 1383.3197046696089 C1048.636373629525 1383.2616165038467 1047.812206644769 1383.0394262030177 1047.2627616778734 1382.854334335053 C1046.5852586143897 1382.6372865510666 1045.968574370453 1382.2935062333745 1045.468013342219 1381.8438410742342 C1044.9490951132543 1381.4047385736678 1044.552281973661 1380.8755919913178 1044.3018841313867 1380.288631639898 C1044.0881988282233 1379.8124274782715 1043.8318489099079 1379.0929109268661 1043.7648284591914 1377.7757373953166 C1043.6913866198054 1376.3473976930327 1043.6732650065646 1375.9188562369416 1043.6732650065646 1372.3056974323526 C1043.6732650065646 1368.6924563087562 1043.6913866198054 1368.2587013155023 1043.7648284591914 1366.8357381742985 C1043.8318489099079 1365.5132155213385 1044.0881988282233 1364.7990464772454 1044.3018841313867 1364.3229383544617 C1044.552281973661 1363.735652762256 1044.9490951132543 1363.2014710005933 1045.474214973388 1362.7675126309675 C1045.980729008839 1362.3178482788765 1046.5912265204202 1361.9739993620108 1047.2689633090426 1361.7572227465207 C1047.818408275938 1361.5720630864319 1048.6487778230392 1361.3499252437946 1050.1688041408336 1361.2916490355926 C1051.8171381103448 1361.2282141695662 1052.3117321637237 1361.21230400368 1056.4812470986515 1361.21230400368 C1060.6571517622706 1361.21230400368 1061.151447839377 1361.2282141695662 1062.7938464940128 1361.2916490355926 C1064.320010191843 1361.3499252437946 1065.1442572577216 1361.5720630864319 1065.6936994310895 1361.7572227465207 C1066.3712490533658 1361.9739993620108 1066.987873702048 1362.3178482788765 1067.4884179691162 1362.7675126309675 C1068.0073995180396 1363.2066296584176 1068.4042107952816 1363.735652762256 1068.6546412287091 1364.3229383544617 C1068.8682008231342 1364.7990464772454 1069.1245684337896 1365.5183741791625 1069.1918998972405 1366.8357381742985 C1069.2650121003753 1368.2640778765824 1069.2834493822302 1368.6924563087562 1069.2834493822302 1372.3056974323526 C1069.2834493822302 1375.9188562369416 1069.2650121003753 1376.3421179778438 1069.1918998972405 1377.7704560660297 Z M1056.358562817901 1365.9919780286002 C1052.1038117702547 1365.9919780286002 1048.6516989216846 1368.983717099451 1048.6516989216846 1372.6715489287333 C1048.6516989216846 1376.359312965892 1052.1038117702547 1379.3509858587167 1056.358562817901 1379.3509858587167 C1060.6135634206746 1379.3509858587167 1064.06536339416 1376.359312965892 1064.06536339416 1372.6715489287333 C1064.06536339416 1368.983717099451 1060.6135634206746 1365.9919780286002 1056.358562817901 1365.9919780286002 Z M1056.358562817901 1377.0043614830747 C1053.5982950032749 1377.0043614830747 1051.359319916034 1375.0640734146843 1051.359319916034 1372.6715489287333 C1051.359319916034 1370.278997003113 1053.5982950032749 1368.3386701963664 1056.358562817901 1368.3386701963664 C1059.1191118476343 1368.3386701963664 1061.3578224809323 1370.278997003113 1061.3578224809323 1372.6715489287333 C1061.3578224809323 1375.0640734146843 1059.1191118476343 1377.0043614830747 1056.358562817901 1377.0043614830747 Z M1064.370159598486 1367.287136874898 C1065.363910466916 1367.287136874898 1066.1695638133967 1366.5889038222626 1066.1695638133967 1365.7277711377149 C1066.1695638133967 1364.866435076795 1065.363910466916 1364.168405400532 1064.370159598486 1364.168405400532 C1063.3765670299515 1364.168405400532 1062.5709118211178 1364.866435076795 1062.5709118211178 1365.7277711377149 C1062.5709118211178 1366.5889038222626 1063.3765670299515 1367.287136874898 1064.370159598486 1367.287136874898 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_5-8c1a3" fill="#1B2437" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_6" class="path firer commentable non-processed" customid="twitter_icn"   datasizewidth="35.2px" datasizeheight="23.7px" dataX="1152.2" dataY="1362.1"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="35.1507568359375" height="23.695068359375" viewBox="1152.1510157555272 1362.1374001714112 35.1507568359375 23.695068359375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_6-8c1a3" d="M1187.3017271330596 1364.942522051505 C1185.9945573719208 1365.4182452118696 1184.6017082583594 1365.733570592521 1183.1495432430743 1365.8866774906976 C1184.6434510094182 1365.1466635063414 1185.783653491024 1363.9837848492384 1186.3197016309482 1362.5821374887687 C1184.9268562420905 1363.2711161094157 1183.3890081496788 1363.7577707497912 1181.75010512919 1364.0293524553194 C1180.4275560676924 1362.861010075834 1178.5425991402744 1362.1374001714112 1176.4862797833075 1362.1374001714112 C1172.4966680228372 1362.1374001714112 1169.2848041309385 1364.824046437104 1169.2848041309385 1368.1176549590218 C1169.2848041309385 1368.591555802527 1169.3331377446716 1369.0472286351405 1169.4517900339872 1369.481030437242 C1163.4607647694409 1369.2386122578614 1158.1595908913414 1366.8563502082202 1154.5983796176897 1363.2273716272434 C1153.9766507285783 1364.122309177292 1153.611961759089 1365.1466635063414 1153.611961759089 1366.2493927942896 C1153.611961759089 1368.319974096999 1154.897161357193 1370.1554264877661 1156.8128778165049 1371.2180583484087 C1155.6550993907056 1371.1998303375171 1154.5192901967653 1370.9209593645503 1153.5570386794438 1370.481688997772 C1153.5570386794438 1370.4999170086635 1153.5570386794438 1370.5236119701335 1153.5570386794438 1370.547306931604 C1153.5570386794438 1373.4526804504526 1156.0549388518152 1375.8659283431011 1159.3305501113143 1376.4218495862733 C1158.743971713823 1376.554906156699 1158.1046673461085 1376.618700159573 1157.4411971027023 1376.618700159573 C1156.9798432336843 1376.618700159573 1156.5140960770013 1376.5968291290608 1156.0769080836744 1376.5166294321548 C1157.0105999720504 1378.884354913472 1159.6601072127005 1380.6250290724558 1162.8104743890328 1380.6815321937931 C1160.3587097897973 1382.272742474222 1157.2456706598139 1383.2314812905865 1153.875592541384 1383.2314812905865 C1153.2846203906392 1383.2314812905865 1152.7178215649933 1383.2096070318776 1152.1510157555272 1383.149459276821 C1155.3431368570289 1384.8573236618402 1159.126236161917 1385.83246494404 1163.2059205624773 1385.83246494404 C1176.4665071952818 1385.83246494404 1183.7163499837163 1376.7189485701317 1183.7163499837163 1368.8193922188339 C1183.7163499837163 1368.5551013948425 1183.7053658333753 1368.2999237692477 1183.689986533017 1368.0465700746108 C1185.1201832476183 1367.2044853628367 1186.3218992059583 1366.1527906315484 1187.3017271330596 1364.942522051505 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_6-8c1a3" fill="#1B2437" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Lateral module" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_2" class="rectangle manualfit firer commentable hidden non-processed" customid="Rectangle 4"   datasizewidth="1366.0px" datasizeheight="944.4px" datasizewidthpx="1366.0000000000002" datasizeheightpx="944.4349357386596" dataX="-0.2" dataY="275.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_2_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_6" class="group firer ie-background commentable non-processed" customid="Texts" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_8" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph 96"   datasizewidth="397.0px" datasizeheight="146.7px" dataX="822.0" dataY="591.4" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_8_0">Prendas dise&ntilde;adas con alta tecnolog&iacute;a y estandares de calidad para mantener tu comodidad en todo momento y sensaci&oacute;n de suave textura en la piel.</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_9" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph 115"   datasizewidth="476.2px" datasizeheight="111.2px" dataX="782.4" dataY="480.2" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_9_0">Solo mant&eacute;n tu confort </span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Rectangle_4" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 8"   datasizewidth="52.0px" datasizeheight="58.0px" datasizewidthpx="51.999999999999886" datasizeheightpx="57.970336283504196" dataX="97.8" dataY="346.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_4_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_5" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 8"   datasizewidth="26.0px" datasizeheight="29.0px" datasizewidthpx="26.000000000000057" datasizeheightpx="28.985168141752013" dataX="129.8" dataY="436.2" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_5_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="shapewrapper-s-Ellipse_1" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_1 non-processed"   datasizewidth="400.0px" datasizeheight="473.2px" datasizewidthpx="400.0" datasizeheightpx="473.22723496738104" dataX="286.8" dataY="311.4" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_1" class="svgContainer" style="width:100%; height:100%;">\
                <g>\
                    <g clip-path="url(#clip-s-Ellipse_1)">\
                            <ellipse id="s-Ellipse_1" class="ellipse shape non-processed-shape manualfit firer ie-background commentable non-processed" customid="Ellipse 1" cx="200.0" cy="236.61361748369052" rx="200.0" ry="236.61361748369052">\
                            </ellipse>\
                    </g>\
                </g>\
                <defs>\
                    <clipPath id="clip-s-Ellipse_1" class="clipPath">\
                            <ellipse cx="200.0" cy="236.61361748369052" rx="200.0" ry="236.61361748369052">\
                            </ellipse>\
                    </clipPath>\
                </defs>\
            </svg>\
            <div class="paddingLayer">\
                <div id="shapert-s-Ellipse_1" class="content firer" >\
                    <div class="valign">\
                        <span id="rtr-s-Ellipse_1_0"></span>\
                    </div>\
                </div>\
            </div>\
        </div>\
\
        <div id="s-Image_2" class="image firer ie-background commentable non-processed" customid="Image"   datasizewidth="566.6px" datasizeheight="379.7px" dataX="97.8" dataY="419.0"   alt="image">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
          		<img src="./images/8fac79fe-4de1-4ebc-a195-4ee224c2576f.png" />\
          	</div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_7" class="group firer ie-background commentable non-processed" customid="Texts" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_10" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph 96"   datasizewidth="397.0px" datasizeheight="146.7px" dataX="184.0" dataY="1135.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_10_0">Pasi&oacute;n por el detalle en la costura y la b&uacute;squeda por el bienstar de quienes vistan las prendas de nuestra marca MACDESIGN mas de cinco a&ntilde;os de mejora cont&iacute;nua.</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_11" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph 115"   datasizewidth="476.2px" datasizeheight="116.0px" dataX="144.4" dataY="980.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_11_0">Pasi&oacute;n por el bienestar y los detalles</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Image_3" class="image firer ie-background commentable non-processed" customid="Image"   datasizewidth="429.6px" datasizeheight="294.7px" dataX="782.4" dataY="965.0"   alt="image">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
          		<img src="./images/a384202a-2308-4a6f-9ea5-1b1e37ce8ce2.jpeg" />\
          	</div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Image_4" class="image firer ie-background commentable non-processed" customid="Image 4"   datasizewidth="124.0px" datasizeheight="128.0px" dataX="0.0" dataY="0.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/7e7337b4-0003-4473-ab8b-a7e9fd4eaadd.jpeg" />\
        	</div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Menu" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Paragraph_2" class="richtext autofit firer click ie-background commentable non-processed" customid="Home"   datasizewidth="131.4px" datasizeheight="34.0px" dataX="170.0" dataY="162.9" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_2_0">NOSOTROS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_3" class="richtext manualfit firer click ie-background commentable non-processed" customid="Blog"   datasizewidth="172.9px" datasizeheight="34.0px" dataX="367.0" dataY="162.9" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_3_0">PRODUCTOS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_4" class="richtext autofit firer click ie-background commentable non-processed" customid="Contact"   datasizewidth="181.0px" datasizeheight="34.0px" dataX="937.8" dataY="164.1" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_4_0">CONT&Aacute;CTANOS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_5" class="richtext manualfit firer click ie-background commentable non-processed" customid="Shop"   datasizewidth="142.7px" datasizeheight="34.2px" dataX="580.9" dataY="162.9" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_5_0">CONSULTA</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_6" class="richtext manualfit firer click commentable non-processed" customid="Shop"   datasizewidth="102.3px" datasizeheight="34.0px" dataX="785.9" dataY="162.9" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_6_0">LOGIN</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Image_1" class="image firer click ie-background commentable non-processed" customid="Image"   datasizewidth="71.7px" datasizeheight="55.0px" dataX="1168.5" dataY="148.8"   alt="image">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
          		<img src="./images/20e0902d-c6ac-4788-a29b-5c18e1732c42.jpg" />\
          	</div>\
          </div>\
        </div>\
\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;