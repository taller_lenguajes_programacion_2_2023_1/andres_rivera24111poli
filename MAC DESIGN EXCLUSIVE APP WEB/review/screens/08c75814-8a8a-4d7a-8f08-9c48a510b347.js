var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1366" deviceHeight="950">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1024" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677131263225.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-08c75814-8a8a-4d7a-8f08-9c48a510b347" class="screen growth-vertical devWeb canvas PORTRAIT firer commentable non-processed" alignment="left" name="PRODUCTOS" width="1366" height="950">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/08c75814-8a8a-4d7a-8f08-9c48a510b347-1677131263225.css" />\
      <div class="freeLayout">\
      <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Background"   datasizewidth="1366.0px" datasizeheight="220.0px" datasizewidthpx="1366.0" datasizeheightpx="219.9569528246875" dataX="0.5" dataY="-0.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_1_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_1" class="richtext manualfit firer ie-background commentable non-processed" customid="Acme"   datasizewidth="239.8px" datasizeheight="78.0px" dataX="585.3" dataY="71.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_1_0">MAC DESGIN</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Footer" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Path_2" class="path firer commentable non-processed" customid="devider"   datasizewidth="1206.0px" datasizeheight="1.0px" dataX="99.0" dataY="3037.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="1206.0" height="0.7229995727539062" viewBox="99.00000000000011 3037.0 1206.0 0.7229995727539062" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_2-08c75" d="M99.00000000000011 3037.0 L1305.000000000001 3037.0 L1305.000000000001 3037.722996483478 L99.00000000000011 3037.722996483478 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_2-08c75" fill="#D9D9D9" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_3" class="path firer commentable non-processed" customid="bg"   datasizewidth="1206.0px" datasizeheight="52.1px" dataX="99.0" dataY="3189.5"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="1206.0" height="52.055755615234375" viewBox="99.00000000000011 3189.5443129974783 1206.0 52.055755615234375" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_3-08c75" d="M99.00000000000011 3189.5443129974783 L1305.000000000001 3189.5443129974783 L1305.000000000001 3241.600059807868 L99.00000000000011 3241.600059807868 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_3-08c75" fill="#EFEFF4" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_7" class="richtext manualfit firer ie-background commentable non-processed" customid="Copyright"   datasizewidth="223.1px" datasizeheight="40.0px" dataX="99.0" dataY="3195.3" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_7_0">&copy; All rights reserved</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="Social" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_4" class="path firer commentable non-processed" customid="facebook_icn"   datasizewidth="18.7px" datasizeheight="29.0px" dataX="1271.3" dataY="3201.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="18.7197265625" height="29.04547119140625" viewBox="1271.2728433228324 3201.042220258888 18.7197265625 29.04547119140625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_4-08c75" d="M1283.7206211751952 3230.087701405503 L1283.7206211751952 3215.563285946202 L1289.2586126269537 3215.563285946202 L1289.99250956234 3210.5580783678647 L1283.7206211751952 3210.5580783678647 L1283.7300241889404 3208.052919261192 C1283.7300241889404 3206.74748980178 1283.9013530961597 3206.048007606488 1286.4911691712186 3206.048007606488 L1289.9533033342998 3206.048007606488 L1289.9533033342998 3201.042232179817 L1284.414524107771 3201.042232179817 C1277.7615650399723 3201.042232179817 1275.419886843397 3203.4702947182077 1275.419886843397 3207.553532815031 L1275.419886843397 3210.558638521777 L1271.2728709793876 3210.558638521777 L1271.2728709793876 3215.5638599500735 L1275.419886843397 3215.5638599500735 L1275.419886843397 3230.087701405503 L1283.7206211751952 3230.087701405503 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_4-08c75" fill="#1B2437" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_5" class="path firer commentable non-processed" customid="instagram_icn"   datasizewidth="31.2px" datasizeheight="25.8px" dataX="1040.8" dataY="3202.7"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="31.2451171875" height="25.818206787109375" viewBox="1040.8404297266761 3202.6558694017685 31.2451171875 25.818206787109375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_5-08c75" d="M1072.0061442512592 3210.246508912409 C1071.9328756105808 3208.8746850560974 1071.6643244950872 3207.931572072719 1071.279696722683 3207.114349064487 C1070.8828817207377 3206.246883789752 1070.272354411529 3205.4702465485725 1069.472481804732 3204.824623767506 C1068.691042755087 3204.16895745757 1067.7447855797734 3203.6594705438165 1066.7069789194702 3203.3367572571624 C1065.7119802754007 3203.0189699393914 1064.5763778024987 3202.797129754048 1062.91601304099 3202.736646212032 C1061.2429954620147 3202.671039723933 1060.7119868457978 3202.6558694017685 1056.4690617314623 3202.6558694017685 C1052.226119855962 3202.6558694017685 1051.695047919787 3202.671039723933 1050.0283567495449 3202.7315240353914 C1048.367853242834 3202.7920075774073 1047.2262828639011 3203.0140455093924 1046.2373294134572 3203.331635080521 C1045.1870887620144 3203.6594705438165 1044.2470332178698 3204.1638329726024 1043.465547609431 3204.824623767506 C1042.6719064314304 3205.4702465485725 1042.0554540502808 3206.252008274719 1041.6645948490805 3207.109417709509 C1041.279937279049 3207.931572072719 1041.0114150300062 3208.8695605711305 1040.9382041222316 3210.2413844274415 C1040.8587925144627 3211.6234580231294 1040.8404297266761 3212.062212424932 1040.8404297266761 3215.567549425361 C1040.8404297266761 3219.0728848869057 1040.8587925144627 3219.5115361834555 1040.9320034222378 3220.8885499273197 C1041.0052143300131 3222.2604007141067 1041.2739758912492 3223.20353985852 1041.6586334612803 3224.0206843836486 C1042.0554540502808 3224.8881750499763 1042.6719064314304 3225.664788438448 1043.465547609431 3226.3103719779624 C1044.2470332178698 3226.966152165343 1045.1932903931831 3227.475547515476 1046.2311268511125 3227.798273113205 C1047.2262828639011 3228.1160958253167 1048.361650680489 3228.3378736858426 1050.0223897746903 3228.398416474907 C1051.6888462886177 3228.4590916080274 1052.220167779921 3228.474064952992 1056.463124554235 3228.474064952992 C1060.70604966857 3228.474064952992 1061.2370601471384 3228.4590916080274 1062.9038276738008 3228.398416474907 C1064.5643470105013 3228.3378736858426 1065.7058866606303 3228.1160958253167 1066.694791689929 3227.798273113205 C1068.7950886199992 3227.1275165030916 1070.4556060943476 3225.755665716304 1071.267509493142 3224.0206843836486 C1071.6519808280036 3223.198504628846 1071.9206883810402 3222.2604007141067 1071.9939570217184 3220.8885499273197 C1072.0670729495566 3219.5115361834555 1072.0855065067071 3219.0728848869057 1072.0855065067071 3215.567549425361 C1072.0855065067071 3212.062212424932 1072.0792564543942 3211.6234580231294 1072.0061442512592 3210.246508912409 Z M1069.1918998972405 3220.7877299175193 C1069.1245684337896 3222.048560968872 1068.8682008231342 3222.7295173046205 1068.6546412287091 3223.1833989398256 C1068.1294096274737 3224.3081695292003 1067.0489551131855 3225.200833266127 1065.6874531034796 3225.6347094044627 C1065.1380072054073 3225.8111763534635 1064.307983124548 3226.02301302093 1062.7875964416996 3226.078394391802 C1061.1392624721882 3226.1390664471537 1060.6449645327298 3226.1540428698872 1056.4753099214238 3226.1540428698872 C1052.3055286702024 3226.1540428698872 1051.8049825407825 3226.1390664471537 1050.1626174084781 3226.078394391802 C1048.6363736295248 3226.02301302093 1047.8122066447688 3225.8111763534635 1047.2627616778732 3225.6347094044627 C1046.5852586143894 3225.427775622614 1045.9685743704529 3225.1000147952113 1045.4680133422187 3224.671303152837 C1044.949095113254 3224.252661969809 1044.5522819736607 3223.748172583062 1044.3018841313865 3223.188563435787 C1044.088198828223 3222.734549456525 1043.8318489099076 3222.048560968872 1043.7648284591912 3220.7927651471928 C1043.6913866198051 3219.4309832808685 1043.6732650065644 3219.0224110183035 1043.6732650065644 3215.577618345824 C1043.6732650065644 3212.132747190241 1043.6913866198051 3211.719204331146 1043.7648284591912 3210.3625484886734 C1043.8318489099076 3209.101652804177 1044.088198828223 3208.420762640457 1044.3018841313865 3207.966840224816 C1044.5522819736607 3207.4069209923387 1044.949095113254 3206.8976310557846 1045.4742149733877 3206.483894297259 C1045.9807290088388 3206.055183424327 1046.59122652042 3205.7273571943383 1047.2689633090424 3205.520681945064 C1047.8184082759378 3205.34415036292 1048.648777823039 3205.1323637091955 1050.1688041408333 3205.076803058294 C1051.8171381103446 3205.016324132931 1052.3117321637235 3205.001155349651 1056.4812470986512 3205.001155349651 C1060.6571517622706 3205.001155349651 1061.151447839377 3205.016324132931 1062.7938464940128 3205.076803058294 C1064.320010191843 3205.1323637091955 1065.1442572577216 3205.34415036292 1065.6936994310895 3205.520681945064 C1066.3712490533658 3205.7273571943383 1066.987873702048 3206.055183424327 1067.4884179691162 3206.483894297259 C1068.0073995180396 3206.902549330246 1068.4042107952816 3207.4069209923387 1068.6546412287091 3207.966840224816 C1068.8682008231342 3208.420762640457 1069.1245684337896 3209.106571078638 1069.1918998972405 3210.3625484886734 C1069.2650121003753 3211.7243303549976 1069.2834493822302 3212.132747190241 1069.2834493822302 3215.577618345824 C1069.2834493822302 3219.0224110183035 1069.2650121003753 3219.4259495900787 1069.1918998972405 3220.7877299175193 Z M1056.3585628179007 3209.558105915172 C1052.1038117702544 3209.558105915172 1048.6516989216843 3212.4104357978636 1048.6516989216843 3215.926421878164 C1048.6516989216843 3219.4423433253214 1052.1038117702544 3222.2946101137536 1056.3585628179007 3222.2946101137536 C1060.6135634206746 3222.2946101137536 1064.06536339416 3219.4423433253214 1064.06536339416 3215.926421878164 C1064.06536339416 3212.4104357978636 1060.6135634206746 3209.558105915172 1056.3585628179007 3209.558105915172 Z M1056.3585628179007 3220.0573338434697 C1053.5982950032746 3220.0573338434697 1051.3593199160339 3218.2074594034084 1051.3593199160339 3215.926421878164 C1051.3593199160339 3213.645358191886 1053.5982950032746 3211.7954468186 1056.3585628179007 3211.7954468186 C1059.1191118476343 3211.7954468186 1061.3578224809323 3213.645358191886 1061.3578224809323 3215.926421878164 C1061.3578224809323 3218.2074594034084 1059.1191118476343 3220.0573338434697 1056.3585628179007 3220.0573338434697 Z M1064.370159598486 3210.7929128928663 C1065.363910466916 3210.7929128928663 1066.1695638133967 3210.1272161345764 1066.1695638133967 3209.3062105485515 C1066.1695638133967 3208.485011063095 1065.363910466916 3207.8195082042366 1064.370159598486 3207.8195082042366 C1063.3765670299515 3207.8195082042366 1062.5709118211178 3208.485011063095 1062.5709118211178 3209.3062105485515 C1062.5709118211178 3210.1272161345764 1063.3765670299515 3210.7929128928663 1064.370159598486 3210.7929128928663 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_5-08c75" fill="#1B2437" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_6" class="path firer commentable non-processed" customid="twitter_icn"   datasizewidth="35.2px" datasizeheight="22.6px" dataX="1152.2" dataY="3205.9"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="35.1507568359375" height="22.590911865234375" viewBox="1152.1510157555272 3205.883143845671 35.1507568359375 22.590911865234375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_6-08c75" d="M1187.3017271330596 3208.5575525336276 C1185.9945573719208 3209.011107925345 1184.6017082583594 3209.3117397601086 1183.1495432430743 3209.457712176343 C1184.6434510094182 3208.7521813960175 1185.783653491024 3207.6434906094723 1186.3197016309482 3206.30715727405 C1184.9268562420905 3206.9640308387784 1183.3890081496788 3207.4280083249228 1181.75010512919 3207.686934854251 C1180.4275560676924 3206.5730349440983 1178.5425991402744 3205.883143845671 1176.4862797833075 3205.883143845671 C1172.4966680228372 3205.883143845671 1169.2848041309385 3208.444597651092 1169.2848041309385 3211.584730458958 C1169.2848041309385 3212.0365484502167 1169.3331377446716 3212.4709878202334 1169.4517900339872 3212.884575306975 C1163.4607647694409 3212.653453340527 1158.1595908913414 3210.382200042136 1154.5983796176897 3206.922324763901 C1153.9766507285783 3207.7755599748243 1153.611961759089 3208.7521813960175 1153.611961759089 3209.803525656344 C1153.611961759089 3211.777621920889 1154.897161357193 3213.527545820821 1156.8128778165049 3214.5406611155145 C1155.6550993907056 3214.5232824942727 1154.5192901967653 3214.257406363109 1153.5570386794438 3213.8386051361067 C1153.5570386794438 3213.855983757349 1153.5570386794438 3213.8785745799673 1153.5570386794438 3213.9011654025862 C1153.5570386794438 3216.6711541990476 1156.0549388518152 3218.971949460777 1159.3305501113143 3219.5019658615297 C1158.743971713823 3219.6288222560634 1158.1046673461085 3219.6896435831995 1157.4411971027023 3219.6896435831995 C1156.9798432336843 3219.6896435831995 1156.5140960770013 3219.668791699924 1156.0769080836744 3219.5923291520044 C1157.0105999720504 3221.849723257718 1159.6601072127005 3223.5092854060595 1162.8104743890328 3223.5631555924756 C1160.3587097897973 3225.080218586617 1157.2456706598139 3225.9942820496562 1153.875592541384 3225.9942820496562 C1153.2846203906392 3225.9942820496562 1152.7178215649933 3225.973427088612 1152.1510157555272 3225.916082101278 C1155.3431368570289 3227.544363346938 1159.126236161917 3228.474064952992 1163.2059205624773 3228.474064952992 C1176.4665071952818 3228.474064952992 1183.7163499837163 3219.785220613935 1183.7163499837163 3212.253768135225 C1183.7163499837163 3212.001792746617 1183.7053658333753 3211.758505899188 1183.689986533017 3211.516957991102 C1185.1201832476183 3210.7141127795817 1186.3218992059583 3209.71142496541 1187.3017271330596 3208.5575525336276 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_6-08c75" fill="#1B2437" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
      <div id="s-Dynamic_Panel_8" class="dynamicpanel firer ie-background commentable non-processed" customid="Dynamic Panel 8" datasizewidth="427.1px" datasizeheight="606.5px" dataX="748.0" dataY="2225.0" >\
        <div id="s-Panel_8" class="panel default firer ie-background commentable non-processed" customid="Panel 8"  datasizewidth="427.1px" datasizeheight="606.5px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="s-Path_14" class="path firer commentable non-processed" customid="Add to cart - icon"   datasizewidth="19.8px" datasizeheight="8.3px" dataX="-649.0" dataY="592.4"  >\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg xmlns="http://www.w3.org/2000/svg" width="19.8482666015625" height="8.301841735839844" viewBox="-649.0000009712757 592.3838822045573 19.8482666015625 8.301841735839844" preserveAspectRatio="none">\
                    	  <g>\
                    	    <defs>\
                    	      <path id="s-Path_14-08c75" d="M-638.6433998015175 593.0653256900206 C-638.6390101007066 593.0653256900206 -638.634620399896 593.0653308890218 -638.6302306990854 593.0653386875235 L-638.6267388916219 593.0653412870242 C-637.1577853748411 593.0653412870242 -635.9350541671754 593.5057746737965 -635.6773587763955 594.0874909182155 L-641.5945757034397 594.0874909182155 C-641.3368803126598 593.5054081442119 -640.1126526160813 593.0653256900206 -638.6433998015175 593.0653256900206 Z M-635.627276280781 594.7689344036789 L-635.627276280781 595.5952870501268 C-635.627276280781 595.783462299109 -635.2613348586433 595.9360087928585 -634.8099938025505 595.9360087928585 C-634.3586527464572 595.9360087928585 -633.9927113243193 595.783462299109 -633.9927113243193 595.5952870501268 L-633.9927113243193 594.7736940892787 C-632.196924628988 594.8340155007174 -630.786334179805 595.4600714268821 -630.786334179805 596.2231600272114 L-630.786334179805 598.5500561988947 C-630.786334179805 599.3530367354421 -632.3482695410332 600.0042818224273 -634.2773435155293 600.0042818224273 L-643.8744266790904 600.0042818224273 C-645.8035006535863 600.0042818224273 -647.3654360148146 599.3530367354421 -647.3654360148146 598.5500561988947 L-647.3654360148146 596.2231600272114 C-647.3654360148146 595.4201794906642 -645.8035006535863 594.7689344036789 -643.8744266790904 594.7689344036789 L-643.2790236236606 594.7689344036789 L-643.2790236236606 595.5952870501268 C-643.2790236236606 595.783462299109 -642.9130822015229 595.9360087928585 -642.4617411454294 595.9360087928585 C-642.0104000893366 595.9360087928585 -641.6444586671989 595.783462299109 -641.6444586671989 595.5952870501268 L-641.6444586671989 594.7689344036789 Z M-638.6435993333728 592.3838822045573 C-641.0156340260503 592.3838822045573 -642.9755356721496 593.1270170382758 -643.2469987609272 594.0874909182155 L-643.8744266790904 594.0874909182155 C-646.705384638353 594.0874909182155 -649.0000009712758 595.043475460618 -649.0000009712758 596.2231600272114 L-649.0000009712758 598.5500561988947 C-649.0000009712758 599.7297459644894 -646.705384638353 600.6857253078906 -643.8744266790904 600.6857253078906 L-634.2773435155293 600.6857253078906 C-631.4463855562672 600.6857253078906 -629.1517692233435 599.7297459644894 -629.1517692233435 598.5500561988947 L-629.1517692233435 596.2231600272114 C-629.1517692233435 595.0788572632938 -631.3108036607694 594.1450360630128 -634.0241375914875 594.0900514263072 C-634.2926077024397 593.1297569119089 -636.2495163707133 592.3844397974361 -638.6241449775064 592.3838978015609 L-638.6245440412165 592.3838978015609 C-638.630929060578 592.3838874035584 -638.6373140799392 592.3838822045573 -638.6435993333728 592.3838822045573 Z "></path>\
                    	    </defs>\
                    	    <g style="mix-blend-mode:normal">\
                    	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_14-08c75" fill="#FFFFFF" fill-opacity="1.0"></use>\
                    	    </g>\
                    	  </g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Group_12" class="group firer ie-background commentable non-processed" customid="Title" datasizewidth="0.0px" datasizeheight="0.0px" >\
                  <div id="s-Image_9" class="image firer click ie-background commentable non-processed" customid="producto8"   datasizewidth="424.3px" datasizeheight="406.4px" dataX="0.7" dataY="-0.0"   alt="image">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                    		<img src="./images/7efbfb9b-7c7b-49cb-9c3c-92dfa2709458.jpeg" />\
                    	</div>\
                    </div>\
                  </div>\
\
                  <div id="s-Paragraph_29" class="richtext manualfit firer click ie-background commentable non-processed" customid="Descrption Text"   datasizewidth="424.3px" datasizeheight="99.3px" dataX="0.7" dataY="457.5" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_29_0">B&oacute;xer corto estilo fondo azul oscuro, composici&oacute;n algod&oacute;n y licra, boxer para usar de manera casual o para entrenamiento deportivo</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_30" class="richtext manualfit firer click ie-background commentable non-processed" customid="Title"   datasizewidth="424.3px" datasizeheight="42.9px" dataX="0.7" dataY="406.4" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_30_0">BOXER DE HOMBRE REF 100143</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_31" class="richtext manualfit firer click ie-background commentable non-processed" customid="Category"   datasizewidth="153.6px" datasizeheight="62.0px" dataX="1.0" dataY="541.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_31_0">25.900 $</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Dynamic_Panel_7" class="dynamicpanel firer ie-background commentable non-processed" customid="Dynamic Panel 7" datasizewidth="435.1px" datasizeheight="606.5px" dataX="99.0" dataY="2225.0" >\
        <div id="s-Panel_7" class="panel default firer ie-background commentable non-processed" customid="Panel 6"  datasizewidth="435.1px" datasizeheight="606.5px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="s-Path_13" class="path firer commentable non-processed" customid="Add to cart - icon"   datasizewidth="19.8px" datasizeheight="8.3px" dataX="-649.0" dataY="592.4"  >\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg xmlns="http://www.w3.org/2000/svg" width="19.8482666015625" height="8.301841735839844" viewBox="-649.0000009712753 592.3838822045573 19.8482666015625 8.301841735839844" preserveAspectRatio="none">\
                    	  <g>\
                    	    <defs>\
                    	      <path id="s-Path_13-08c75" d="M-638.643399801517 593.0653256900206 C-638.6390101007062 593.0653256900206 -638.6346203998955 593.0653308890218 -638.6302306990849 593.0653386875235 L-638.6267388916215 593.0653412870242 C-637.1577853748406 593.0653412870242 -635.9350541671749 593.5057746737965 -635.6773587763951 594.0874909182155 L-641.5945757034392 594.0874909182155 C-641.3368803126593 593.5054081442119 -640.1126526160808 593.0653256900206 -638.643399801517 593.0653256900206 Z M-635.6272762807805 594.7689344036789 L-635.6272762807805 595.5952870501268 C-635.6272762807805 595.783462299109 -635.2613348586428 595.9360087928585 -634.80999380255 595.9360087928585 C-634.3586527464568 595.9360087928585 -633.9927113243189 595.783462299109 -633.9927113243189 595.5952870501268 L-633.9927113243189 594.7736940892787 C-632.1969246289875 594.8340155007174 -630.7863341798045 595.4600714268821 -630.7863341798045 596.2231600272114 L-630.7863341798045 598.5500561988947 C-630.7863341798045 599.3530367354421 -632.3482695410328 600.0042818224273 -634.2773435155289 600.0042818224273 L-643.87442667909 600.0042818224273 C-645.8035006535858 600.0042818224273 -647.3654360148141 599.3530367354421 -647.3654360148141 598.5500561988947 L-647.3654360148141 596.2231600272114 C-647.3654360148141 595.4201794906642 -645.8035006535858 594.7689344036789 -643.87442667909 594.7689344036789 L-643.2790236236601 594.7689344036789 L-643.2790236236601 595.5952870501268 C-643.2790236236601 595.783462299109 -642.9130822015225 595.9360087928585 -642.461741145429 595.9360087928585 C-642.0104000893361 595.9360087928585 -641.6444586671985 595.783462299109 -641.6444586671985 595.5952870501268 L-641.6444586671985 594.7689344036789 Z M-638.6435993333723 592.3838822045573 C-641.0156340260498 592.3838822045573 -642.9755356721491 593.1270170382758 -643.2469987609268 594.0874909182155 L-643.87442667909 594.0874909182155 C-646.7053846383526 594.0874909182155 -649.0000009712753 595.043475460618 -649.0000009712753 596.2231600272114 L-649.0000009712753 598.5500561988947 C-649.0000009712753 599.7297459644894 -646.7053846383526 600.6857253078906 -643.87442667909 600.6857253078906 L-634.2773435155289 600.6857253078906 C-631.4463855562667 600.6857253078906 -629.1517692233431 599.7297459644894 -629.1517692233431 598.5500561988947 L-629.1517692233431 596.2231600272114 C-629.1517692233431 595.0788572632938 -631.3108036607689 594.1450360630128 -634.024137591487 594.0900514263072 C-634.2926077024392 593.1297569119089 -636.2495163707129 592.3844397974361 -638.6241449775059 592.3838978015609 L-638.624544041216 592.3838978015609 C-638.6309290605775 592.3838874035584 -638.6373140799387 592.3838822045573 -638.6435993333723 592.3838822045573 Z "></path>\
                    	    </defs>\
                    	    <g style="mix-blend-mode:normal">\
                    	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_13-08c75" fill="#FFFFFF" fill-opacity="1.0"></use>\
                    	    </g>\
                    	  </g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Group_10" class="group firer ie-background commentable non-processed" customid="Title" datasizewidth="0.0px" datasizeheight="0.0px" >\
                  <div id="s-Image_8" class="image firer click ie-background commentable non-processed" customid="producto7"   datasizewidth="436.3px" datasizeheight="421.5px" dataX="-0.3" dataY="-0.0"   alt="image">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                    		<img src="./images/1d827088-3994-4dc0-99f3-2d17b61003dd.jpeg" />\
                    	</div>\
                    </div>\
                  </div>\
\
                  <div id="s-Paragraph_26" class="richtext manualfit firer click ie-background commentable non-processed" customid="Descrption Text"   datasizewidth="436.3px" datasizeheight="103.0px" dataX="-0.3" dataY="474.5" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_26_0">B&oacute;xer medio estilo signos blancos, composici&oacute;n algod&oacute;n y licra, boxer para usar de manera casual o para entrenamiento deportivo</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_27" class="richtext manualfit firer click ie-background commentable non-processed" customid="Title"   datasizewidth="436.3px" datasizeheight="44.5px" dataX="-0.3" dataY="421.5" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_27_0">BOXER DE HOMBRE REF 100475</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_28" class="richtext manualfit firer click ie-background commentable non-processed" customid="Category"   datasizewidth="107.1px" datasizeheight="62.0px" dataX="-0.3" dataY="552.5" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_28_0">27.900 $</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Dynamic_Panel_6" class="dynamicpanel firer ie-background commentable non-processed" customid="Dynamic Panel 6" datasizewidth="413.0px" datasizeheight="578.5px" dataX="748.0" dataY="1572.0" >\
        <div id="s-Panel_6" class="panel default firer ie-background commentable non-processed" customid="Panel 6"  datasizewidth="413.0px" datasizeheight="578.5px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="s-Path_12" class="path firer commentable non-processed" customid="Add to cart - icon"   datasizewidth="19.8px" datasizeheight="8.3px" dataX="-649.0" dataY="592.4"  >\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg xmlns="http://www.w3.org/2000/svg" width="19.8482666015625" height="8.301841735839844" viewBox="-649.0000009712761 592.3838822045573 19.8482666015625 8.301841735839844" preserveAspectRatio="none">\
                    	  <g>\
                    	    <defs>\
                    	      <path id="s-Path_12-08c75" d="M-638.643399801518 593.0653256900206 C-638.6390101007071 593.0653256900206 -638.6346203998964 593.0653308890218 -638.6302306990858 593.0653386875235 L-638.6267388916224 593.0653412870242 C-637.1577853748415 593.0653412870242 -635.9350541671758 593.5057746737965 -635.677358776396 594.0874909182155 L-641.5945757034401 594.0874909182155 C-641.3368803126602 593.5054081442119 -640.1126526160817 593.0653256900206 -638.643399801518 593.0653256900206 Z M-635.6272762807814 594.7689344036789 L-635.6272762807814 595.5952870501268 C-635.6272762807814 595.783462299109 -635.2613348586437 595.9360087928585 -634.8099938025509 595.9360087928585 C-634.3586527464577 595.9360087928585 -633.9927113243198 595.783462299109 -633.9927113243198 595.5952870501268 L-633.9927113243198 594.7736940892787 C-632.1969246289884 594.8340155007174 -630.7863341798054 595.4600714268821 -630.7863341798054 596.2231600272114 L-630.7863341798054 598.5500561988947 C-630.7863341798054 599.3530367354421 -632.3482695410337 600.0042818224273 -634.2773435155298 600.0042818224273 L-643.8744266790909 600.0042818224273 C-645.8035006535868 600.0042818224273 -647.365436014815 599.3530367354421 -647.365436014815 598.5500561988947 L-647.365436014815 596.2231600272114 C-647.365436014815 595.4201794906642 -645.8035006535868 594.7689344036789 -643.8744266790909 594.7689344036789 L-643.279023623661 594.7689344036789 L-643.279023623661 595.5952870501268 C-643.279023623661 595.783462299109 -642.9130822015234 595.9360087928585 -642.4617411454299 595.9360087928585 C-642.010400089337 595.9360087928585 -641.6444586671994 595.783462299109 -641.6444586671994 595.5952870501268 L-641.6444586671994 594.7689344036789 Z M-638.6435993333732 592.3838822045573 C-641.0156340260507 592.3838822045573 -642.97553567215 593.1270170382758 -643.2469987609277 594.0874909182155 L-643.8744266790909 594.0874909182155 C-646.7053846383535 594.0874909182155 -649.0000009712762 595.043475460618 -649.0000009712762 596.2231600272114 L-649.0000009712762 598.5500561988947 C-649.0000009712762 599.7297459644894 -646.7053846383535 600.6857253078906 -643.8744266790909 600.6857253078906 L-634.2773435155298 600.6857253078906 C-631.4463855562676 600.6857253078906 -629.151769223344 599.7297459644894 -629.151769223344 598.5500561988947 L-629.151769223344 596.2231600272114 C-629.151769223344 595.0788572632938 -631.3108036607698 594.1450360630128 -634.0241375914879 594.0900514263072 C-634.2926077024401 593.1297569119089 -636.2495163707138 592.3844397974361 -638.6241449775068 592.3838978015609 L-638.624544041217 592.3838978015609 C-638.6309290605784 592.3838874035584 -638.6373140799396 592.3838822045573 -638.6435993333732 592.3838822045573 Z "></path>\
                    	    </defs>\
                    	    <g style="mix-blend-mode:normal">\
                    	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_12-08c75" fill="#FFFFFF" fill-opacity="1.0"></use>\
                    	    </g>\
                    	  </g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Group_9" class="group firer ie-background commentable non-processed" customid="Title" datasizewidth="0.0px" datasizeheight="0.0px" >\
                  <div id="s-Image_7" class="image firer click ie-background commentable non-processed" customid="producto6"   datasizewidth="414.3px" datasizeheight="384.1px" dataX="0.7" dataY="-0.0"   alt="image">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                    		<img src="./images/3bdfcc5e-1328-472a-a3a5-d67e7df36a02.jpeg" />\
                    	</div>\
                    </div>\
                  </div>\
\
                  <div id="s-Paragraph_23" class="richtext manualfit firer click ie-background commentable non-processed" customid="Descrption Text"   datasizewidth="414.1px" datasizeheight="102.7px" dataX="1.0" dataY="428.4" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_23_0">B&oacute;xer medio estilo azul rey, composici&oacute;n algod&oacute;n y licra, boxer para usar de manera casual o para entrenamiento deportivo</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_24" class="richtext manualfit firer click ie-background commentable non-processed" customid="Title"   datasizewidth="414.3px" datasizeheight="44.4px" dataX="0.7" dataY="384.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_24_0">BOXER DE HOMBRE REF 100973</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_25" class="richtext manualfit firer click ie-background commentable non-processed" customid="Category"   datasizewidth="150.2px" datasizeheight="62.0px" dataX="0.7" dataY="531.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_25_0">27.900 $</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Dynamic_Panel_5" class="dynamicpanel firer ie-background commentable non-processed" customid="Dynamic Panel 5" datasizewidth="435.1px" datasizeheight="578.5px" dataX="99.0" dataY="1572.0" >\
        <div id="s-Panel_5" class="panel default firer ie-background commentable non-processed" customid="Panel 3"  datasizewidth="435.1px" datasizeheight="578.5px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="s-Path_11" class="path firer commentable non-processed" customid="Add to cart - icon"   datasizewidth="19.8px" datasizeheight="8.3px" dataX="-649.0" dataY="592.4"  >\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg xmlns="http://www.w3.org/2000/svg" width="19.8482666015625" height="8.301841735839844" viewBox="-649.0000009712754 592.3838822045573 19.8482666015625 8.301841735839844" preserveAspectRatio="none">\
                    	  <g>\
                    	    <defs>\
                    	      <path id="s-Path_11-08c75" d="M-638.643399801517 593.0653256900206 C-638.6390101007062 593.0653256900206 -638.6346203998955 593.0653308890218 -638.6302306990849 593.0653386875235 L-638.6267388916215 593.0653412870242 C-637.1577853748406 593.0653412870242 -635.9350541671749 593.5057746737965 -635.6773587763951 594.0874909182155 L-641.5945757034392 594.0874909182155 C-641.3368803126593 593.5054081442119 -640.1126526160808 593.0653256900206 -638.643399801517 593.0653256900206 Z M-635.6272762807805 594.7689344036789 L-635.6272762807805 595.5952870501268 C-635.6272762807805 595.783462299109 -635.2613348586428 595.9360087928585 -634.80999380255 595.9360087928585 C-634.3586527464568 595.9360087928585 -633.9927113243189 595.783462299109 -633.9927113243189 595.5952870501268 L-633.9927113243189 594.7736940892787 C-632.1969246289875 594.8340155007174 -630.7863341798045 595.4600714268821 -630.7863341798045 596.2231600272114 L-630.7863341798045 598.5500561988947 C-630.7863341798045 599.3530367354421 -632.3482695410328 600.0042818224273 -634.2773435155289 600.0042818224273 L-643.87442667909 600.0042818224273 C-645.8035006535858 600.0042818224273 -647.3654360148141 599.3530367354421 -647.3654360148141 598.5500561988947 L-647.3654360148141 596.2231600272114 C-647.3654360148141 595.4201794906642 -645.8035006535858 594.7689344036789 -643.87442667909 594.7689344036789 L-643.2790236236601 594.7689344036789 L-643.2790236236601 595.5952870501268 C-643.2790236236601 595.783462299109 -642.9130822015225 595.9360087928585 -642.461741145429 595.9360087928585 C-642.0104000893361 595.9360087928585 -641.6444586671985 595.783462299109 -641.6444586671985 595.5952870501268 L-641.6444586671985 594.7689344036789 Z M-638.6435993333723 592.3838822045573 C-641.0156340260498 592.3838822045573 -642.9755356721491 593.1270170382758 -643.2469987609268 594.0874909182155 L-643.87442667909 594.0874909182155 C-646.7053846383526 594.0874909182155 -649.0000009712753 595.043475460618 -649.0000009712753 596.2231600272114 L-649.0000009712753 598.5500561988947 C-649.0000009712753 599.7297459644894 -646.7053846383526 600.6857253078906 -643.87442667909 600.6857253078906 L-634.2773435155289 600.6857253078906 C-631.4463855562667 600.6857253078906 -629.1517692233431 599.7297459644894 -629.1517692233431 598.5500561988947 L-629.1517692233431 596.2231600272114 C-629.1517692233431 595.0788572632938 -631.3108036607689 594.1450360630128 -634.024137591487 594.0900514263072 C-634.2926077024392 593.1297569119089 -636.2495163707129 592.3844397974361 -638.6241449775059 592.3838978015609 L-638.624544041216 592.3838978015609 C-638.6309290605775 592.3838874035584 -638.6373140799387 592.3838822045573 -638.6435993333723 592.3838822045573 Z "></path>\
                    	    </defs>\
                    	    <g style="mix-blend-mode:normal">\
                    	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_11-08c75" fill="#FFFFFF" fill-opacity="1.0"></use>\
                    	    </g>\
                    	  </g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Group_8" class="group firer ie-background commentable non-processed" customid="Title" datasizewidth="0.0px" datasizeheight="0.0px" >\
                  <div id="s-Image_6" class="image firer click ie-background commentable non-processed" customid="producto5"   datasizewidth="434.3px" datasizeheight="385.9px" dataX="0.7" dataY="-0.0"   alt="image">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                    		<img src="./images/ad973fdb-2289-4e5a-96fa-ce7acc7de818.jpeg" />\
                    	</div>\
                    </div>\
                  </div>\
\
                  <div id="s-Paragraph_20" class="richtext manualfit firer click ie-background commentable non-processed" customid="Descrption Text"   datasizewidth="434.3px" datasizeheight="94.3px" dataX="0.7" dataY="434.3" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_20_0">B&oacute;xer corto estilo gris rat&oacute;n, composici&oacute;n algod&oacute;n y licra, boxer para usar de manera casual o para entrenamiento deportivo</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_21" class="richtext manualfit firer click ie-background commentable non-processed" customid="Title"   datasizewidth="434.3px" datasizeheight="40.8px" dataX="0.7" dataY="385.9" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_21_0">BOXER DE HOMBRE REF 100354</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_22" class="richtext manualfit firer click ie-background commentable non-processed" customid="Category"   datasizewidth="102.7px" datasizeheight="62.0px" dataX="0.7" dataY="528.6" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_22_0">25.900 $</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Dynamic_Panel_4" class="dynamicpanel firer ie-background commentable non-processed" customid="Dynamic Panel 4" datasizewidth="413.1px" datasizeheight="558.5px" dataX="747.9" dataY="933.0" >\
        <div id="s-Panel_4" class="panel default firer ie-background commentable non-processed" customid="Panel 4"  datasizewidth="413.1px" datasizeheight="558.5px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="s-Path_10" class="path firer commentable non-processed" customid="Add to cart - icon"   datasizewidth="19.8px" datasizeheight="8.3px" dataX="-649.0" dataY="592.4"  >\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg xmlns="http://www.w3.org/2000/svg" width="19.8482666015625" height="8.301841735839844" viewBox="-649.0000009712757 592.3838822045573 19.8482666015625 8.301841735839844" preserveAspectRatio="none">\
                    	  <g>\
                    	    <defs>\
                    	      <path id="s-Path_10-08c75" d="M-638.6433998015175 593.0653256900206 C-638.6390101007066 593.0653256900206 -638.634620399896 593.0653308890218 -638.6302306990854 593.0653386875235 L-638.6267388916219 593.0653412870242 C-637.1577853748411 593.0653412870242 -635.9350541671754 593.5057746737965 -635.6773587763955 594.0874909182155 L-641.5945757034397 594.0874909182155 C-641.3368803126598 593.5054081442119 -640.1126526160813 593.0653256900206 -638.6433998015175 593.0653256900206 Z M-635.627276280781 594.7689344036789 L-635.627276280781 595.5952870501268 C-635.627276280781 595.783462299109 -635.2613348586433 595.9360087928585 -634.8099938025505 595.9360087928585 C-634.3586527464572 595.9360087928585 -633.9927113243193 595.783462299109 -633.9927113243193 595.5952870501268 L-633.9927113243193 594.7736940892787 C-632.196924628988 594.8340155007174 -630.786334179805 595.4600714268821 -630.786334179805 596.2231600272114 L-630.786334179805 598.5500561988947 C-630.786334179805 599.3530367354421 -632.3482695410332 600.0042818224273 -634.2773435155293 600.0042818224273 L-643.8744266790904 600.0042818224273 C-645.8035006535863 600.0042818224273 -647.3654360148146 599.3530367354421 -647.3654360148146 598.5500561988947 L-647.3654360148146 596.2231600272114 C-647.3654360148146 595.4201794906642 -645.8035006535863 594.7689344036789 -643.8744266790904 594.7689344036789 L-643.2790236236606 594.7689344036789 L-643.2790236236606 595.5952870501268 C-643.2790236236606 595.783462299109 -642.9130822015229 595.9360087928585 -642.4617411454294 595.9360087928585 C-642.0104000893366 595.9360087928585 -641.6444586671989 595.783462299109 -641.6444586671989 595.5952870501268 L-641.6444586671989 594.7689344036789 Z M-638.6435993333728 592.3838822045573 C-641.0156340260503 592.3838822045573 -642.9755356721496 593.1270170382758 -643.2469987609272 594.0874909182155 L-643.8744266790904 594.0874909182155 C-646.705384638353 594.0874909182155 -649.0000009712758 595.043475460618 -649.0000009712758 596.2231600272114 L-649.0000009712758 598.5500561988947 C-649.0000009712758 599.7297459644894 -646.705384638353 600.6857253078906 -643.8744266790904 600.6857253078906 L-634.2773435155293 600.6857253078906 C-631.4463855562672 600.6857253078906 -629.1517692233435 599.7297459644894 -629.1517692233435 598.5500561988947 L-629.1517692233435 596.2231600272114 C-629.1517692233435 595.0788572632938 -631.3108036607694 594.1450360630128 -634.0241375914875 594.0900514263072 C-634.2926077024397 593.1297569119089 -636.2495163707133 592.3844397974361 -638.6241449775064 592.3838978015609 L-638.6245440412165 592.3838978015609 C-638.630929060578 592.3838874035584 -638.6373140799392 592.3838822045573 -638.6435993333728 592.3838822045573 Z "></path>\
                    	    </defs>\
                    	    <g style="mix-blend-mode:normal">\
                    	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_10-08c75" fill="#FFFFFF" fill-opacity="1.0"></use>\
                    	    </g>\
                    	  </g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Group_7" class="group firer ie-background commentable non-processed" customid="Title" datasizewidth="0.0px" datasizeheight="0.0px" >\
                  <div id="s-Image_5" class="image firer click ie-background commentable non-processed" customid="producto3"   datasizewidth="414.3px" datasizeheight="369.4px" dataX="0.7" dataY="-0.0"   alt="image">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                    		<img src="./images/98ca5ae9-3e57-42b4-821e-66c80c22742c.jpeg" />\
                    	</div>\
                    </div>\
                  </div>\
\
                  <div id="s-Paragraph_17" class="richtext manualfit firer click ie-background commentable non-processed" customid="Descrption Text"   datasizewidth="414.3px" datasizeheight="92.0px" dataX="0.7" dataY="415.8" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_17_0">B&oacute;xer medio fondo palo de rosa, composici&oacute;n algod&oacute;n y licra, boxer para usar de manera casual o para entrenamiento deportivo</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_18" class="richtext manualfit firer click ie-background commentable non-processed" customid="Title"   datasizewidth="414.3px" datasizeheight="39.0px" dataX="0.7" dataY="369.4" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_18_0">BOXER DE HOMBRE REF 100367</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_19" class="richtext manualfit firer click ie-background commentable non-processed" customid="Category"   datasizewidth="136.7px" datasizeheight="62.0px" dataX="0.7" dataY="506.1" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_19_0">29.900 $</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Dynamic_Panel_3" class="dynamicpanel firer ie-background commentable non-processed" customid="Dynamic Panel 3" datasizewidth="435.1px" datasizeheight="558.5px" dataX="99.0" dataY="933.0" >\
        <div id="s-Panel_3" class="panel default firer ie-background commentable non-processed" customid="Panel 3"  datasizewidth="435.1px" datasizeheight="558.5px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="s-Path_9" class="path firer commentable non-processed" customid="Add to cart - icon"   datasizewidth="19.8px" datasizeheight="8.3px" dataX="-649.0" dataY="592.4"  >\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg xmlns="http://www.w3.org/2000/svg" width="19.8482666015625" height="8.301841735839844" viewBox="-649.0000009712755 592.3838822045573 19.8482666015625 8.301841735839844" preserveAspectRatio="none">\
                    	  <g>\
                    	    <defs>\
                    	      <path id="s-Path_9-08c75" d="M-638.6433998015173 593.0653256900206 C-638.6390101007064 593.0653256900206 -638.6346203998958 593.0653308890218 -638.6302306990851 593.0653386875235 L-638.6267388916217 593.0653412870242 C-637.1577853748408 593.0653412870242 -635.9350541671752 593.5057746737965 -635.6773587763953 594.0874909182155 L-641.5945757034394 594.0874909182155 C-641.3368803126596 593.5054081442119 -640.112652616081 593.0653256900206 -638.6433998015173 593.0653256900206 Z M-635.6272762807807 594.7689344036789 L-635.6272762807807 595.5952870501268 C-635.6272762807807 595.783462299109 -635.2613348586431 595.9360087928585 -634.8099938025503 595.9360087928585 C-634.358652746457 595.9360087928585 -633.9927113243191 595.783462299109 -633.9927113243191 595.5952870501268 L-633.9927113243191 594.7736940892787 C-632.1969246289877 594.8340155007174 -630.7863341798047 595.4600714268821 -630.7863341798047 596.2231600272114 L-630.7863341798047 598.5500561988947 C-630.7863341798047 599.3530367354421 -632.348269541033 600.0042818224273 -634.2773435155291 600.0042818224273 L-643.8744266790902 600.0042818224273 C-645.8035006535861 600.0042818224273 -647.3654360148143 599.3530367354421 -647.3654360148143 598.5500561988947 L-647.3654360148143 596.2231600272114 C-647.3654360148143 595.4201794906642 -645.8035006535861 594.7689344036789 -643.8744266790902 594.7689344036789 L-643.2790236236604 594.7689344036789 L-643.2790236236604 595.5952870501268 C-643.2790236236604 595.783462299109 -642.9130822015227 595.9360087928585 -642.4617411454292 595.9360087928585 C-642.0104000893364 595.9360087928585 -641.6444586671987 595.783462299109 -641.6444586671987 595.5952870501268 L-641.6444586671987 594.7689344036789 Z M-638.6435993333725 592.3838822045573 C-641.0156340260501 592.3838822045573 -642.9755356721494 593.1270170382758 -643.246998760927 594.0874909182155 L-643.8744266790902 594.0874909182155 C-646.7053846383528 594.0874909182155 -649.0000009712755 595.043475460618 -649.0000009712755 596.2231600272114 L-649.0000009712755 598.5500561988947 C-649.0000009712755 599.7297459644894 -646.7053846383528 600.6857253078906 -643.8744266790902 600.6857253078906 L-634.2773435155291 600.6857253078906 C-631.4463855562669 600.6857253078906 -629.1517692233433 599.7297459644894 -629.1517692233433 598.5500561988947 L-629.1517692233433 596.2231600272114 C-629.1517692233433 595.0788572632938 -631.3108036607691 594.1450360630128 -634.0241375914873 594.0900514263072 C-634.2926077024395 593.1297569119089 -636.2495163707131 592.3844397974361 -638.6241449775061 592.3838978015609 L-638.6245440412163 592.3838978015609 C-638.6309290605777 592.3838874035584 -638.637314079939 592.3838822045573 -638.6435993333725 592.3838822045573 Z "></path>\
                    	    </defs>\
                    	    <g style="mix-blend-mode:normal">\
                    	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_9-08c75" fill="#FFFFFF" fill-opacity="1.0"></use>\
                    	    </g>\
                    	  </g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Group_6" class="group firer ie-background commentable non-processed" customid="Title" datasizewidth="0.0px" datasizeheight="0.0px" >\
                  <div id="s-Image_4" class="image firer click ie-background commentable non-processed" customid="producto3"   datasizewidth="434.3px" datasizeheight="376.2px" dataX="0.7" dataY="-0.0"   alt="image">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                    		<img src="./images/6de72d63-ef3f-4338-ae7e-80ea3e64234c.jpeg" />\
                    	</div>\
                    </div>\
                  </div>\
\
                  <div id="s-Paragraph_14" class="richtext manualfit firer click ie-background commentable non-processed" customid="Descrption Text"   datasizewidth="434.3px" datasizeheight="91.9px" dataX="0.7" dataY="423.5" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_14_0">B&oacute;xer medio estilo lineas grises, composici&oacute;n algod&oacute;n y licra, boxer para usar de manera casual o para entrenamiento deportivo</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_15" class="richtext manualfit firer click ie-background commentable non-processed" customid="Title"   datasizewidth="434.3px" datasizeheight="39.7px" dataX="0.7" dataY="376.2" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_15_0">BOXER DE HOMBRE REF 100794</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_16" class="richtext manualfit firer click ie-background commentable non-processed" customid="Category"   datasizewidth="108.7px" datasizeheight="62.0px" dataX="0.7" dataY="515.5" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_16_0">25.900 $</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Dynamic_Panel_2" class="dynamicpanel firer ie-background commentable non-processed" customid="Dynamic Panel 2" datasizewidth="413.1px" datasizeheight="564.5px" dataX="747.9" dataY="281.4" >\
        <div id="s-Panel_2" class="panel default firer ie-background commentable non-processed" customid="Panel 2"  datasizewidth="413.1px" datasizeheight="564.5px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="s-Path_8" class="path firer commentable non-processed" customid="Add to cart - icon"   datasizewidth="19.8px" datasizeheight="8.3px" dataX="-649.0" dataY="592.4"  >\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg xmlns="http://www.w3.org/2000/svg" width="19.8482666015625" height="8.301841735839844" viewBox="-649.0000009712757 592.3838822045573 19.8482666015625 8.301841735839844" preserveAspectRatio="none">\
                    	  <g>\
                    	    <defs>\
                    	      <path id="s-Path_8-08c75" d="M-638.6433998015175 593.0653256900206 C-638.6390101007066 593.0653256900206 -638.634620399896 593.0653308890218 -638.6302306990854 593.0653386875235 L-638.6267388916219 593.0653412870242 C-637.1577853748411 593.0653412870242 -635.9350541671754 593.5057746737965 -635.6773587763955 594.0874909182155 L-641.5945757034397 594.0874909182155 C-641.3368803126598 593.5054081442119 -640.1126526160813 593.0653256900206 -638.6433998015175 593.0653256900206 Z M-635.627276280781 594.7689344036789 L-635.627276280781 595.5952870501268 C-635.627276280781 595.783462299109 -635.2613348586433 595.9360087928585 -634.8099938025505 595.9360087928585 C-634.3586527464572 595.9360087928585 -633.9927113243193 595.783462299109 -633.9927113243193 595.5952870501268 L-633.9927113243193 594.7736940892787 C-632.196924628988 594.8340155007174 -630.786334179805 595.4600714268821 -630.786334179805 596.2231600272114 L-630.786334179805 598.5500561988947 C-630.786334179805 599.3530367354421 -632.3482695410332 600.0042818224273 -634.2773435155293 600.0042818224273 L-643.8744266790904 600.0042818224273 C-645.8035006535863 600.0042818224273 -647.3654360148146 599.3530367354421 -647.3654360148146 598.5500561988947 L-647.3654360148146 596.2231600272114 C-647.3654360148146 595.4201794906642 -645.8035006535863 594.7689344036789 -643.8744266790904 594.7689344036789 L-643.2790236236606 594.7689344036789 L-643.2790236236606 595.5952870501268 C-643.2790236236606 595.783462299109 -642.9130822015229 595.9360087928585 -642.4617411454294 595.9360087928585 C-642.0104000893366 595.9360087928585 -641.6444586671989 595.783462299109 -641.6444586671989 595.5952870501268 L-641.6444586671989 594.7689344036789 Z M-638.6435993333728 592.3838822045573 C-641.0156340260503 592.3838822045573 -642.9755356721496 593.1270170382758 -643.2469987609272 594.0874909182155 L-643.8744266790904 594.0874909182155 C-646.705384638353 594.0874909182155 -649.0000009712758 595.043475460618 -649.0000009712758 596.2231600272114 L-649.0000009712758 598.5500561988947 C-649.0000009712758 599.7297459644894 -646.705384638353 600.6857253078906 -643.8744266790904 600.6857253078906 L-634.2773435155293 600.6857253078906 C-631.4463855562672 600.6857253078906 -629.1517692233435 599.7297459644894 -629.1517692233435 598.5500561988947 L-629.1517692233435 596.2231600272114 C-629.1517692233435 595.0788572632938 -631.3108036607694 594.1450360630128 -634.0241375914875 594.0900514263072 C-634.2926077024397 593.1297569119089 -636.2495163707133 592.3844397974361 -638.6241449775064 592.3838978015609 L-638.6245440412165 592.3838978015609 C-638.630929060578 592.3838874035584 -638.6373140799392 592.3838822045573 -638.6435993333728 592.3838822045573 Z "></path>\
                    	    </defs>\
                    	    <g style="mix-blend-mode:normal">\
                    	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_8-08c75" fill="#FFFFFF" fill-opacity="1.0"></use>\
                    	    </g>\
                    	  </g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Title" datasizewidth="0.0px" datasizeheight="0.0px" >\
                  <div id="s-Image_2" class="image firer click ie-background commentable non-processed" customid="producto2"   datasizewidth="410.3px" datasizeheight="378.4px" dataX="0.7" dataY="-0.0"   alt="image">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                    		<img src="./images/f7d7b805-1c85-47b6-9999-4da211f3115c.jpeg" />\
                    	</div>\
                    </div>\
                  </div>\
\
                  <div id="s-Paragraph_8" class="richtext manualfit firer click ie-background commentable non-processed" customid="Descrption Text"   datasizewidth="410.3px" datasizeheight="92.5px" dataX="0.7" dataY="426.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_8_0">B&oacute;xer corto estilo gris fondo, composici&oacute;n algod&oacute;n y licra, boxer para usar de manera casual o para entrenamiento deportivo</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_9" class="richtext manualfit firer click ie-background commentable non-processed" customid="Title"   datasizewidth="410.3px" datasizeheight="40.0px" dataX="0.7" dataY="378.4" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_9_0">BOXER DE HOMBRE REF 100465</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_13" class="richtext manualfit firer click ie-background commentable non-processed" customid="Category"   datasizewidth="124.3px" datasizeheight="62.0px" dataX="0.7" dataY="518.4" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_13_0">29.900 $</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Dynamic_Panel_1" class="dynamicpanel firer ie-background commentable non-processed" customid="Dynamic Panel 1" datasizewidth="435.1px" datasizeheight="564.5px" dataX="99.0" dataY="281.4" >\
        <div id="s-Panel_1" class="panel default firer ie-background commentable non-processed" customid="Panel 1"  datasizewidth="435.1px" datasizeheight="564.5px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="s-Path_7" class="path firer commentable non-processed" customid="Add to cart - icon"   datasizewidth="19.8px" datasizeheight="8.3px" dataX="-649.0" dataY="592.4"  >\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg xmlns="http://www.w3.org/2000/svg" width="19.8482666015625" height="8.301841735839844" viewBox="-649.000000971275 592.3838822045573 19.8482666015625 8.301841735839844" preserveAspectRatio="none">\
                    	  <g>\
                    	    <defs>\
                    	      <path id="s-Path_7-08c75" d="M-638.6433998015166 593.0653256900206 C-638.6390101007057 593.0653256900206 -638.6346203998951 593.0653308890218 -638.6302306990844 593.0653386875235 L-638.626738891621 593.0653412870242 C-637.1577853748402 593.0653412870242 -635.9350541671745 593.5057746737965 -635.6773587763946 594.0874909182155 L-641.5945757034387 594.0874909182155 C-641.3368803126589 593.5054081442119 -640.1126526160804 593.0653256900206 -638.6433998015166 593.0653256900206 Z M-635.62727628078 594.7689344036789 L-635.62727628078 595.5952870501268 C-635.62727628078 595.783462299109 -635.2613348586424 595.9360087928585 -634.8099938025496 595.9360087928585 C-634.3586527464563 595.9360087928585 -633.9927113243184 595.783462299109 -633.9927113243184 595.5952870501268 L-633.9927113243184 594.7736940892787 C-632.1969246289871 594.8340155007174 -630.786334179804 595.4600714268821 -630.786334179804 596.2231600272114 L-630.786334179804 598.5500561988947 C-630.786334179804 599.3530367354421 -632.3482695410323 600.0042818224273 -634.2773435155284 600.0042818224273 L-643.8744266790895 600.0042818224273 C-645.8035006535854 600.0042818224273 -647.3654360148137 599.3530367354421 -647.3654360148137 598.5500561988947 L-647.3654360148137 596.2231600272114 C-647.3654360148137 595.4201794906642 -645.8035006535854 594.7689344036789 -643.8744266790895 594.7689344036789 L-643.2790236236597 594.7689344036789 L-643.2790236236597 595.5952870501268 C-643.2790236236597 595.783462299109 -642.913082201522 595.9360087928585 -642.4617411454285 595.9360087928585 C-642.0104000893357 595.9360087928585 -641.644458667198 595.783462299109 -641.644458667198 595.5952870501268 L-641.644458667198 594.7689344036789 Z M-638.6435993333719 592.3838822045573 C-641.0156340260494 592.3838822045573 -642.9755356721487 593.1270170382758 -643.2469987609263 594.0874909182155 L-643.8744266790895 594.0874909182155 C-646.7053846383521 594.0874909182155 -649.0000009712749 595.043475460618 -649.0000009712749 596.2231600272114 L-649.0000009712749 598.5500561988947 C-649.0000009712749 599.7297459644894 -646.7053846383521 600.6857253078906 -643.8744266790895 600.6857253078906 L-634.2773435155284 600.6857253078906 C-631.4463855562663 600.6857253078906 -629.1517692233426 599.7297459644894 -629.1517692233426 598.5500561988947 L-629.1517692233426 596.2231600272114 C-629.1517692233426 595.0788572632938 -631.3108036607684 594.1450360630128 -634.0241375914866 594.0900514263072 C-634.2926077024388 593.1297569119089 -636.2495163707124 592.3844397974361 -638.6241449775055 592.3838978015609 L-638.6245440412156 592.3838978015609 C-638.630929060577 592.3838874035584 -638.6373140799383 592.3838822045573 -638.6435993333719 592.3838822045573 Z "></path>\
                    	    </defs>\
                    	    <g style="mix-blend-mode:normal">\
                    	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_7-08c75" fill="#FFFFFF" fill-opacity="1.0"></use>\
                    	    </g>\
                    	  </g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Group_11" class="group firer ie-background commentable non-processed" customid="Title" datasizewidth="0.0px" datasizeheight="0.0px" >\
                  <div id="s-Image_3" class="image firer click ie-background commentable non-processed" customid="producto1"   datasizewidth="430.3px" datasizeheight="373.6px" dataX="0.7" dataY="-0.0"   alt="image">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                    		<img src="./images/6b9a1659-cc5b-4775-82de-51bc7821e3e9.jpeg" />\
                    	</div>\
                    </div>\
                  </div>\
\
                  <div id="s-Paragraph_10" class="richtext manualfit firer click ie-background commentable non-processed" customid="Descrption Text"   datasizewidth="430.3px" datasizeheight="91.3px" dataX="0.7" dataY="420.5" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_10_0">B&oacute;xer medio oceano, composici&oacute;n algod&oacute;n y licra, boxer para usar de manera casual o para entrenamiento deportivo</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_11" class="richtext manualfit firer click ie-background commentable non-processed" customid="Title"   datasizewidth="430.3px" datasizeheight="39.5px" dataX="0.7" dataY="373.6" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_11_0">BOXER DE HOMBRE REF 100350</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Paragraph_12" class="richtext manualfit firer click ie-background commentable non-processed" customid="Category"   datasizewidth="104.0px" datasizeheight="62.0px" dataX="0.7" dataY="511.8" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_12_0">25.900 $</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Image_10" class="image firer ie-background commentable non-processed" customid="Image 4"   datasizewidth="124.0px" datasizeheight="128.0px" dataX="-0.0" dataY="0.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/7e7337b4-0003-4473-ab8b-a7e9fd4eaadd.jpeg" />\
        	</div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Menu" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Paragraph_2" class="richtext autofit firer click ie-background commentable non-processed" customid="Home"   datasizewidth="131.4px" datasizeheight="34.0px" dataX="170.0" dataY="162.9" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_2_0">NOSOTROS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_3" class="richtext manualfit firer click ie-background commentable non-processed" customid="Blog"   datasizewidth="172.9px" datasizeheight="34.0px" dataX="367.0" dataY="162.9" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_3_0">PRODUCTOS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_4" class="richtext autofit firer click ie-background commentable non-processed" customid="Contact"   datasizewidth="181.0px" datasizeheight="34.0px" dataX="937.8" dataY="164.1" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_4_0">CONT&Aacute;CTANOS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_5" class="richtext manualfit firer click ie-background commentable non-processed" customid="Shop"   datasizewidth="142.7px" datasizeheight="34.2px" dataX="580.9" dataY="162.9" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_5_0">CONSULTA</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_6" class="richtext manualfit firer click commentable non-processed" customid="Shop"   datasizewidth="102.3px" datasizeheight="34.0px" dataX="785.9" dataY="162.9" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_6_0">LOGIN</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Image_1" class="image firer click ie-background commentable non-processed" customid="Image"   datasizewidth="71.7px" datasizeheight="55.0px" dataX="1168.5" dataY="148.8"   alt="image">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
          		<img src="./images/20e0902d-c6ac-4788-a29b-5c18e1732c42.jpg" />\
          	</div>\
          </div>\
        </div>\
\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;